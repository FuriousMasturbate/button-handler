#ifndef BUTTON_H_
#define BUTTON_H_

#define BUTTON_DEBOUNCE_CYCLE_THRESHOLD 4
#define BUTTON_MINIMUM_CYCLES_FOR_TAP_EVENT 2
#define BUTTON_MAXIMUM_CYCLES_FOR_TAP_EVENT 8
#define BUTTON_CYCLES_FOR_HOLD_EVENT 20

void
debounce (void);

void
button_event_handler (void);

#endif /* BUTTON_H_ */
