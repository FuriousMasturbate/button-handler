# Button handler task

A task that handles debouncing one button, and executing
events when the button is tapped or held down.

## Functions

### `void debounce (void)`

Checks the input pin for changes in button state
and debounces the button.

Recommended execution period: 4 milliseconds.

### `void button_event_handler (void)`

Checks the time between changes of button's debounce state;

If the button is depressed after a few cycles of being pressed (which is between two configurable thresholds),
the function `button_tap_event` will be executed.

If the button is not depressed after a few cycles of being pressed (which is a configurable threshold).
the function `button_hold_event` will be executed.

Recommended execution period: 20 milliseconds.
