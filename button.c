#include "button.h"
#include "iomacros.h"
#include <stdint.h>

// Static function prototypes:
static void
button_tap_event (void);

static void
button_hold_event (void);

static _Bool button_debounced_state =
  0; // button_debounced_state = 1 means button is pressed

void
debounce (void)
{
	static uint8_t consecutive_changed_state_cycles = 0;
	_Bool button_raw_state =
	  !READ_BUTTON; // button_raw_state = 1 means button is pressed

	if (button_raw_state != button_debounced_state)
	{
		consecutive_changed_state_cycles++;
		if (consecutive_changed_state_cycles >= BUTTON_DEBOUNCE_CYCLE_THRESHOLD)
			button_debounced_state = button_raw_state;
	}

	else
	{
		consecutive_changed_state_cycles = 0;
	}
}

void
button_event_handler (void)
{
	static uint8_t consecutive_button_pressed_cycles = 0;

	if (!button_debounced_state) // if button is depressed
	{
		if (consecutive_button_pressed_cycles >=
		      BUTTON_MINIMUM_CYCLES_FOR_TAP_EVENT &&
		    consecutive_button_pressed_cycles <=
		      BUTTON_MAXIMUM_CYCLES_FOR_TAP_EVENT)
			button_tap_event ();

		consecutive_button_pressed_cycles = 0;
	}

	else if (consecutive_button_pressed_cycles == BUTTON_CYCLES_FOR_HOLD_EVENT)
	{
		button_hold_event ();

		/*
		    set cycles to 21 so the function will
		    do nothing after button_hold_event
		    is initiated, until button is depressed.
		*/
		consecutive_button_pressed_cycles = BUTTON_CYCLES_FOR_HOLD_EVENT + 1;
	}

	else if (consecutive_button_pressed_cycles ==
	         BUTTON_CYCLES_FOR_HOLD_EVENT + 1)
	{
		// do nothing;
	}

	else
	{
		consecutive_button_pressed_cycles++;
	}
}

static void
button_tap_event (void)
{
	// Execute tap-event tasks here.
}

static void
button_hold_event (void)
{
	// Execute hold-event tasks here.
}
